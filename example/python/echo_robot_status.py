#!/usr/bin/env python3  
# -*- coding: utf-8 -*- 
#----------------------------------------------------------------------------
# Created By  : Nicholas, NCS Product and Platforms, RobotManager
# Created Date: 22 Dec 2021
# version ='1.0'
# ---------------------------------------------------------------------------
"""To verify whether robot status has been successfully published to RobotManager server"""  
# ---------------------------------------------------------------------------

import paho.mqtt.client as mqtt
import json
from jproperties import Properties
import os
import sys 

companyId = 'c0000000-c000-c000-c000-c00000000001'
robotId = '664e0340-6f46-4872-b392-8b1a25dc324d'
configs = Properties()

abspath = os.path.abspath(sys.argv[0])
dname = os.path.dirname(abspath)
os.chdir(dname)

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    topic = "/robot/status/" + companyId
    client.subscribe(topic)
    print("Subscribed to " + topic)
    print("Listening to robotId " + robotId)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    try:
        server_status=str(msg.payload.decode("utf-8"))
        server_status=json.loads(server_status)
        robot_id = server_status['robotId']
        id = server_status['id']
        if(robot_id== robotId and id):
            print(msg.topic+" "+str(msg.payload))
    except:
        print("An exception occurred")
    #print(msg.topic+" "+str(msg.payload))

if __name__ == "__main__":
    try:
        with open('../../conf/root-config.properties', 'rb') as config_file:
            configs.load(config_file)
            companyId = configs.get('companyId').data
            robotId = configs.get('robotId').data
    except:
        print("Error loading properties file, make sure you key in correct directory")

    client = mqtt.Client(client_id="", clean_session=True, userdata=None, transport="tcp")
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set(username="Admin",password="Admin123")
    client.connect("dev.mqtt.robotmanager.io", 1883, 60)

    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.
    client.loop_forever()