package com.example.rm_android_framework;

import java.util.HashMap;
import java.util.Map;
import kotlin.Pair;

public class Info  {
    //Mqtt topic
    public static Map<String, Pair<String, Integer>> publish_topic=new HashMap();
    public static Map<String, Pair<String, Integer>> subscribe_topic=new HashMap();
    //Adapter IP
    public static String broker = "tcp://10.176.36.97:5555"; //"tcp://192.168.9.103:1883";
    public static String mapid = "6afcf6ce-52a3-4959-98b5-5213b79d41c6";
    public static String publish_clientId = "publish_clientId";
    public static String subscribe_clientId = "subscribe_clientId";
    //robot ID only use for RTSP streaming
    public static String robot_id = "6efeefaebf584fa8be7576a81a516a45";
    public static String Def_robot_state = "{"
            + "\"batteryPct\": 100,"
            + "\"state\": 2"
            + "}";

    public static final long ROBOT_STATUS_PULSE_MS = 1000L; //1S

    public void init_topic(){
        //publish_topic example:
        // int qos_level = 0;
        // Map.put("Topic_Nickname",new Pair<>("/Real/Topic_Name",qos_level))
        publish_topic.put("event",new Pair<>("/robot/event",2));
        publish_topic.put("status",new Pair<>("/robot/status",0));
        subscribe_topic.put("task",new Pair<>("/rm/task",2));
        subscribe_topic.put("mode",new Pair<>("/rm/mode",2));
        subscribe_topic.put("move",new Pair<>("/rm/move",0));
        subscribe_topic.put("stream",new Pair<>("/rm/vc/request",2));
    }

}