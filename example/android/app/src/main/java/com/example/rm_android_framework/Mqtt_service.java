package com.example.rm_android_framework;

import android.util.Log;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import com.alibaba.fastjson.JSONObject;
import static com.example.rm_android_framework.Info.*;

import java.util.Map;

import kotlin.Pair;

public class Mqtt_service {
    private MqttClient pubClient;
    private MqttClient subClient;
    Rm_task rm=new Rm_task();
    private Mqtt_service(){}
    private static Mqtt_service instance=new Mqtt_service();
    public static Mqtt_service getInstance(){
        return instance;
    }

    public void pubConnection(){
        try {
            Log.i("pubConnection","Connecting to " + broker);
            // set up Mqtt connection parameters
            pubClient = new MqttClient(broker, publish_clientId, new MemoryPersistence());
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(false);
            connOpts.setKeepAliveInterval(20);
            connOpts.setConnectionTimeout(50);
            connOpts.setAutomaticReconnect(true);
            // start connection
            pubClient.connect(connOpts);
            Log.i("pubConnection","Mqtt pubConnection Success");
        }catch (MqttException a) {
            Log.e("pubConnection","Mqtt pubConnection Fail: " + a);
        }
    }

    public void publish(String topic, int qos, JSONObject content) {
        try {
            MqttMessage message = new MqttMessage();
            message.setQos(qos);
            message.setPayload(content.toString().getBytes());
            pubClient.publish(topic,message);
            Log.i("publish","publish Success");

        } catch (MqttException  me) {
            Log.e("publish","publish Fail");
            Log.e("publish_problem","msg " + me.getMessage());
            Log.e("publish_problem","loc " + me.getLocalizedMessage());
            Log.e("publish_problem","cause " + me.getCause());
            Log.e("publish_problem","excep " + me);
        }

    }

    public void subConnection()
    {
        try {
            Log.i("subConnection","Connecting to " + broker);
            // set up Mqtt connection parameters
            subClient = new MqttClient(broker, subscribe_clientId, new MemoryPersistence());
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(false);
            options.setConnectionTimeout(10);
            options.setKeepAliveInterval(60);
            options.setAutomaticReconnect(true);
            subClient.setCallback(new MqttCallbackExtended() {
                @Override
                //recall when connection Complete
                public void connectComplete(boolean reconnect, String serverURI) {
                    //subscribe to all publish topic from publish_topic map
                    for (Map.Entry<String, Pair<String, Integer>> entry : publish_topic.entrySet()) {
                        subscribe(entry.getValue().getFirst(), entry.getValue().getSecond());
                    }
                    //subscribe to all topic from subscribe_topic map
                    for (Map.Entry<String, Pair<String, Integer>> entry : subscribe_topic.entrySet()) {
                        subscribe(entry.getValue().getFirst(), entry.getValue().getSecond());
                    }
                }
                //recall when connection Lost
                public void connectionLost(Throwable cause) {
                    Log.e("subConnection", "Mqtt connection lost: " + String.valueOf(cause));
                    cause.printStackTrace();
                }
                //recall when subscribe message Arrived
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    rm.msg_deal_task(topic, message);
                }
                public void deliveryComplete(IMqttDeliveryToken token) {
                }
            });
            // start connection
            subClient.connect(options);
        }
        catch (Exception e) {
            Log.e("subConnect",String.valueOf(e));
        }
    }

    public void subscribe(String topic,int qos) {
        try {
            subClient.subscribe(topic, qos);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
