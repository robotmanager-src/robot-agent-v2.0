package com.example.rm_android_framework;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.pedro.rtsp.rtsp.Protocol;
import com.pedro.rtsp.utils.ConnectCheckerRtsp;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.pedro.rtplibrary.rtsp.RtspCamera1;

import static com.example.rm_android_framework.Info.*;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import kotlin.Pair;

public class MainActivity extends AppCompatActivity implements  ConnectCheckerRtsp, SurfaceHolder.Callback{
    private ExecutorService es = Executors.newSingleThreadExecutor();
    public static RtspCamera1 rtspCamera1;
    private SurfaceView surfaceView;
    private Disposable sendRobotStatusDisposable;
    private Mqtt_service mqtt = Mqtt_service.getInstance();
    private Robot_action robot_action = new Robot_action();
    private Rm_task rm_task = new Rm_task();

    Info info = new Info();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        info.init_topic();
        //RTSP Streaming Setup
        surfaceView = findViewById(R.id.sfv);
        rtspCamera1 = new RtspCamera1(surfaceView, this);
        rtspCamera1.setReTries(10);
        rtspCamera1.setProtocol(Protocol.TCP);
        surfaceView.getHolder().addCallback(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    protected void onStop() {
        super.onStop();
    }

    //star task and robot mission
    public void task_start(){
        es.execute(()->{
            Log.e("es","----------------");

            //start Mqtt connection
            Mqtt_service mqtt=Mqtt_service.getInstance();
            mqtt.pubConnection();
            mqtt.subConnection();

            //execute publishRobotStatus() every 1S
            sendRobotStatusDisposable =
                    Observable.interval(ROBOT_STATUS_PULSE_MS, ROBOT_STATUS_PULSE_MS, TimeUnit.MILLISECONDS)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    aLong -> publishRobotStatus()
                            );
        });
    }

    private void publishRobotStatus() {
        int [] test = robot_action.getPosition();
        Log.e("TEST", "TEST_X = " + test[0] + " TEST_Y = " + test[0]);
        mqtt.publish(publish_topic.get("status").getFirst(), publish_topic.get("status").getSecond(), rm_task.status_content(robot_action.getRobotbattery(), robot_action.getPosition()));
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {
        rtspCamera1.stopStream();
    }

    @Override
    public void onAuthErrorRtsp() {

    }

    @Override
    public void onAuthSuccessRtsp() {

    }

    @Override
    public void onConnectionFailedRtsp(@NonNull String s) {

    }

    @Override
    public void onConnectionStartedRtsp(@NonNull String s) {

    }

    @Override
    public void onConnectionSuccessRtsp() {

    }

    @Override
    public void onDisconnectRtsp() {

    }

    @Override
    public void onNewBitrateRtsp(long l) {

    }
}