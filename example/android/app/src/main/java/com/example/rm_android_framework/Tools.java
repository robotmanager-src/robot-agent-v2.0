package com.example.rm_android_framework;

public class Tools {
    //Temi机器人的距离倍数
    final double x_multiple=19.0;
    final double y_multiple=20.56;
    //坐标原点的像素点
    final int origin_x=743;
    final int origin_y=621;
    //只针对一张地图的转换方法
    public int[] coordinate_transform(float x,float y){
        int[] a=new int[2];
        int x1= Math.abs((int) (x*x_multiple));
        int y1= Math.abs((int)(y*y_multiple));
        //划分四个象限去计算校准坐标
        if (x>=0){
            a[0]=origin_x-x1;
        }else {
            a[0]=origin_x+x1;
        }
        if(y>=0){
            a[1]=origin_y+y1;
        }else {
            a[1]=origin_y-y1;
        }
        return a;
    }


//    @Override
//    public void onReceive(Context context, Intent intent) {
//        //此处进行接收广播
//        Mqtt_service mqtt=Mqtt_service.getInstance();
//        if(mqtt.pubConnection()!=-1){
////            mqtt.publish();
//        }
//    }
}
