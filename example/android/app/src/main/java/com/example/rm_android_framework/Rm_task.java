package com.example.rm_android_framework;

import static com.example.rm_android_framework.MainActivity.rtspCamera1;
import android.util.Log;
import com.alibaba.fastjson.JSONObject;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import static com.example.rm_android_framework.Info.*;

//Handle the messages received by Mqtt subscribed topic
//and instruct the robot to do the action specified by the messages received
public class Rm_task {

    Robot_action robot_action = new Robot_action();
    private boolean teleoperation = false;

    //The following three function: status_content,event_content and logs_content
    //used to encapsulate the data format that the robot needs to publish
    public JSONObject status_content(int battery,int[] position){
        JSONObject robot_state_json = JSONObject.parseObject(Def_robot_state);
        robot_state_json.put("batteryPct",battery);
        if(position[0] != -1) {
            JSONObject map_pose = new JSONObject();
            map_pose.put("mapId", Info.mapid);
            map_pose.put("x", position[0]);
            map_pose.put("y", position[1]);
            map_pose.put("heading", 180);
            robot_state_json.put("mapPose",map_pose);
        }
        return robot_state_json;
    }

    public JSONObject event_content(){
        JSONObject robot_state_json = JSONObject.parseObject(Def_robot_state);
        return robot_state_json;
    }
    public JSONObject logs_content(){
        JSONObject robot_state_json = JSONObject.parseObject(Def_robot_state);
        return robot_state_json;
    }

    //distinguish messages topic and call Robot_action to send instruction to robot
    public void msg_deal_task(String topic, MqttMessage message){
        //distinguish topic
        Log.d("msg_deal_task", "topic = " + topic);
        Log.e("msg_deal_task", "message = " + message.toString());
        JSONObject object = JSONObject.parseObject(message.toString());
        // /rm/task
        if (topic.equals(subscribe_topic.get("task").getFirst()))
        {
            int task_type = object.getInteger("scheduleType");

            // this will depend on how the SKILL is setup on robotmanager
            String location = object.getString("parameters");
            JSONObject object2 = JSONObject.parseObject(location);
            location=object2.getString("action");

            switch (task_type)
            {
                case 4://Start mission

                    break;

                case 2://Pause mission

                    break;

                case 1://Abort

                    break;
            }
        }
        // /rm/mode take control of the robot
        else if(topic.equals(subscribe_topic.get("mode").getFirst()))
        {
            try{
                teleoperation = object.getBoolean("teleoperation");
            }catch (Exception e) {
                Log.e("msg_deal_task",String.valueOf(e));
            }

        }
        // /rm/move control robot movement, teleoperation
        else if(topic.equals(subscribe_topic.get("move").getFirst()))
        {
            if(teleoperation)
            {
                Log.e("msg_deal_task","Start moving");

            }
        }
        // /rm/vc/request start streaming
        else if(topic.equals(subscribe_topic.get("stream").getFirst()))
        {
            boolean enableStreaming = false;
            try{
                enableStreaming= object.getBoolean("enableVideo") && object.getBoolean("enableAudio");
            }catch (Exception e) {
                Log.e("Error",String.valueOf(e));
            }
            if(enableStreaming) {
                if (!rtspCamera1.isStreaming()) {
                    Log.e("test","isRecording = "+String.valueOf(rtspCamera1.isRecording()) + ", prepareVideo = "+String.valueOf(rtspCamera1.prepareVideo()));
                    if (rtspCamera1.prepareAudio() && rtspCamera1.prepareVideo()) {
                        rtspCamera1.startStream("rtsp://video.dev.robotmanager.io:8554/" + robot_id);
                    }
                    else {
                        Log.e("error while startStream","This device cant init encoders, this could be for 2 reasons: The encoder selected doesnt support any configuration setted or your device hasnt a H264 or AAC encoder (in this case you can see log error valid encoder not found)");
                    }
                }
            }
            else {
                if(rtspCamera1.isStreaming())
                {
                    rtspCamera1.stopStream();
                }
            }
        }
    }
}
