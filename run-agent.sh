#!/bin/bash
file_url=$(cat file_url.txt)

# Extract the filename from the URL
filename=$(echo "$file_url" | awk -F'/' '{print $NF}' | sed 's/\.zip$//')

rm data.db
echo Running $filename
java -jar $filename --config ${1:-'./conf'}
