#!/bin/bash

# Function to check if a command is available
command_exists() {
  command -v "$1" >/dev/null 2>&1
}

# Check if curl is installed
if command_exists curl; then
  echo "curl is already installed."
else
  # Attempt to install curl
  echo "Installing curl..."
  if command_exists apt-get; then
    sudo apt-get update
    sudo apt-get install -y curl
  elif command_exists yum; then
    sudo yum install -y curl
  else
    echo "Error: Unable to install curl. Please install it manually."
    exit 1
  fi
fi

git pull # Get latest URL
url=$(cat file_url.txt) # Read the URL from the text file
curl -O $url

# Extract the contents 
if command_exists unzip; then
  if [[ $url == *".zip" ]]; then
    echo "Extracting zip file..."
    unzip $(basename $url)
    rm $(basename $url) # Remove the zip file after successful extraction
    rm data.db # Delete old db to support new jar
  else
    echo "The downloaded file is not a zip file."
  fi
else
  echo "Error: unzip command not found. Please install unzip manually."
fi
