#!/bin/bash
SERVER_URL=video.dev.robotmanager.io:8554
source conf/root-config.properties

if [[ "$1" == "stop" ]]
then
    echo Stopping streaming service...
    for pid in `cat stream.pid`; do
        kill -9 $pid
    done
    rm stream.pid
else
    # Delete the pid file
    rm stream.pid

    # Streaming video to robotmanager
    file="conf/root-config.properties"
    while IFS='=' read -r key value
    do
        key=$(echo $key | tr '.' '_')        
        if [[ "$key" == *"video_streamSource"* ]]  # If the key contains video
        then
            STREAM_SOURCE=$value
        elif [[ "$key" == *"video_input"* ]]
        then
            INPUT=$value
            ffmpeg -rtsp_transport tcp -use_wallclock_as_timestamps 1 -fflags +genpts -i $INPUT \
                -fflags flush_packets -max_delay 2 -acodec copy -vcodec copy \
                -f rtsp -rtsp_transport tcp rtsp://$robotId:$accessKey@$SERVER_URL/$STREAM_SOURCE &
            pid=$!
            echo $pid >> stream.pid
        fi
    done < "$file"
fi